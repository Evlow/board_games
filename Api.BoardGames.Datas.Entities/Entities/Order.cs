﻿using System;
using System.Collections.Generic;

namespace Api.BoardGames.Datas.Entities.Entities
{
    public partial class Order
    {
        public Order()
        {
            ProductOrders = new HashSet<ProductOrder>();
        }

        public int Id { get; set; }
        public string? NumberOrdered { get; set; }
        public DateOnly? DateOrder { get; set; }
        public double? TotalPrice { get; set; }
        public int? StateId { get; set; }
        public int? UserId { get; set; }

        public virtual State? State { get; set; }
        public virtual User? User { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
    }
}

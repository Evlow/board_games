﻿using System;
using System.Collections.Generic;

namespace Api.BoardGames.Datas.Entities.Entities
{
    public partial class State
    {
        public State()
        {
            Orders = new HashSet<Order>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Api.BoardGames.Datas.Entities.Entities
{
    public partial class ProductOrder
    {
        public int Id { get; set; }
        public double? QuantityOrdered { get; set; }
        public int? ProductId { get; set; }
        public int? OrdersId { get; set; }

        public virtual Order? Orders { get; set; }
        public virtual Product? Product { get; set; }
    }
}
